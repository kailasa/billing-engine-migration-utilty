package com.vuclip.billing.engine.util.service;

import com.vuclip.billing.engine.util.entity.SubscriptionPlanEntity;
import com.vuclip.billing.engine.util.entity.UserSubscriptionEntity;
import com.vuclip.billing.engine.util.model.BaasStatus;
import com.vuclip.billing.engine.util.model.Country;
import com.vuclip.billing.engine.util.model.Currency;
import com.vuclip.billing.engine.util.model.Partner;
import com.vuclip.billing.engine.util.model.SubscriptionStatus;
import com.vuclip.billing.engine.util.repository.UserSubscriptionRepository;
import com.vuclip.rabbitmq.RabbitMQEventPublisher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MigrationServiceTest {
//
//    @InjectMocks
//    private MigrationService service;
//    @Mock
//    private UserSubscriptionRepository repository;
//    @Mock
//    private RabbitMQEventPublisher publisher;
//    @Mock
//    private RabbitAdmin rabbitAdmin;
//    private MockMvc mockMvc;
//
//    @Before
//    public void setup() {
//        ReflectionTestUtils.setField(service, "queueName", "BILLING_ENGINE");
//    }
//
//    @Test
//    public void testProcess_NoData_NothingPublished() {
//        when(repository.findByMigratedToUBSFalseAndSubscriptionEndDateGreaterThanEqualAndPartnerNotInAndLastUpdatedOnLessThanEqual(any(),any(), any())).thenReturn(new ArrayList<>());
//        service.process();
//        verify(rabbitAdmin, atLeastOnce()).declareQueue(any());
//        verifyNoMoreInteractions(publisher);
//    }
//
//    @Test
//    public void testProcess_DataFound_RelevantRecordsPublished() {
//        UserSubscriptionEntity paidUserSubscriptionEntity = getUserSubscriptionEntity(10);
//        UserSubscriptionEntity freeUserSubscriptionEntity = getUserSubscriptionEntity(0);
//        freeUserSubscriptionEntity.setBaasStatus(BaasStatus.SUSPEND);
//        UserSubscriptionEntity parkingUserSubscriptionEntity = getUserSubscriptionEntity(0);
//        parkingUserSubscriptionEntity.setBaasStatus(BaasStatus.PARKING);
//        when(repository.findByMigratedToUBSFalseAndSubscriptionEndDateGreaterThanEqualAndPartnerNotInAndLastUpdatedOnLessThanEqual(any(Date.class), any(),any())).thenReturn(Arrays.asList(paidUserSubscriptionEntity, freeUserSubscriptionEntity, parkingUserSubscriptionEntity));
//        service.process();
//        verify(rabbitAdmin, atLeastOnce()).declareQueue(any());
//        verify(publisher, times(3)).publishMessage(anyString(), anyString());
//    }
//
//    private UserSubscriptionEntity getUserSubscriptionEntity(double amount) {
//        UserSubscriptionEntity userSubscriptionEntity = new UserSubscriptionEntity();
//        userSubscriptionEntity.setId(1L);
//        userSubscriptionEntity.setPartner(Partner.AIRTEL);
//        userSubscriptionEntity.setSubscriptionStartDate(new Date(1556108329321L));
//        userSubscriptionEntity.setSubscriptionEndDate(new Date(1556108329322L));
//        userSubscriptionEntity.setLastUpdatedOn(new Date(1556108329321L));
//        userSubscriptionEntity.setSubscriptionPlan(getSubscriptionPlanEntity(amount));
//        userSubscriptionEntity.setUserId("userId");
//        userSubscriptionEntity.setMsisdn("partnerUserId");
//        userSubscriptionEntity.setPartner(Partner.AIRTEL);
//        userSubscriptionEntity.setSubscriptionStatus(SubscriptionStatus.ACTIVE);
//        return userSubscriptionEntity;
//    }
//
//    private SubscriptionPlanEntity getSubscriptionPlanEntity(double amount) {
//        SubscriptionPlanEntity subscriptionPlanEntity = new SubscriptionPlanEntity();
//        subscriptionPlanEntity.setId(1L);
//        subscriptionPlanEntity.setAmount(amount);
//        subscriptionPlanEntity.setCurrency(Currency.INR);
//        subscriptionPlanEntity.setCountry(Country.IN);
//        subscriptionPlanEntity.setValidityDays(10);
//        return subscriptionPlanEntity;
//    }
}