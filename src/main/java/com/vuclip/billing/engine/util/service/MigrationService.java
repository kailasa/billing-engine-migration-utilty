package com.vuclip.billing.engine.util.service;

import com.google.gson.Gson;
import com.vuclip.billing.engine.util.entity.UserSubscriptionEntity;
import com.vuclip.billing.engine.util.model.BaasStatus;
import com.vuclip.billing.engine.util.model.Partner;
import com.vuclip.billing.engine.util.repository.UserSubscriptionRepository;
import com.vuclip.privilege.domain.UserSubscriptionVO;
import com.vuclip.rabbitmq.RabbitMQEventPublisher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static com.vuclip.privilege.domain.IdentityType.USER_ID;
import static com.vuclip.privilege.domain.Source.BILLING_ENGINE;

@Service
public class MigrationService {

    private static final Logger LOGGER = LogManager.getLogger(MigrationService.class.getName());

    @Autowired(required = false)
    private  UserSubscriptionRepository repository;
    @Autowired(required = false)
    private  RabbitMQEventPublisher publisher;
    @Autowired(required = false)
    private  RabbitAdmin rabbitAdmin;

    @Value("${privilege.queue.name}")
    private String queueName;

    @Value("${user.subscription.lastUpdatedOnStart}")
    private long lastUpdatedOnStart;

    @Value("${user.subscription.lastUpdatedOnEnd}")
    private long lastUpdatedOnEnd;

    @Value("${rabbitmq.backup.path}")
    private String filePathToRead;

    private final Gson gson = new Gson();

    private BufferedWriter bufferedWriter = null;

    public void fetch(){
        Date date = new Date();
        LOGGER.info("starting migration on {} ", date);
        List<UserSubscriptionEntity> userSubscriptionEntities =
                repository.findByPartnerAndSubscriptionEndDateGreaterThanEqual(Partner.MANDIRI, date   );
        LOGGER.info("Processing : {} records", userSubscriptionEntities.size());
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(new File(filePathToRead + "/data.txt")));
            userSubscriptionEntities.forEach(userSubscriptionEntity -> {
                UserSubscriptionVO userSubscriptionVO = convertToUserSubscriptionVO(userSubscriptionEntity);
                writeToFile(userSubscriptionVO);
            });
        } catch (IOException e) {
            LOGGER.error("Exception occured: ", e);
        } finally {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                LOGGER.error("Exception occured: ", e);
            }
        }
        LOGGER.info("Published eligible records");
    }

    public void publish(){
        LOGGER.info("started to publish to rabbitmq");
        declareQueue();

        try (Stream<Path> paths = Files.walk(Paths.get(filePathToRead))) {
            paths.filter(Files::isRegularFile)
            .forEach((Path e) -> {
                LOGGER.info("Processing file: {}", e);
                try {
                    Files.lines(e, StandardCharsets.UTF_8)
                            .filter(str -> !StringUtils.isEmpty(str))
                            .forEach(this::publishToQueue);
                } catch (IOException e1) {
                    LOGGER.error("Exception occured while processing records from file {} exception:", e, e1);
                }
            });
        } catch (IOException e) {
            LOGGER.error("Exception occured while reading files from directory:", e);
        }

        LOGGER.info("Published eligible records");
    }

    private void declareQueue() {
        LOGGER.info("Declaring queue: {}", queueName);
        Queue queue = new Queue(queueName, true);
        rabbitAdmin.declareQueue(queue);
        LOGGER.info("Declared queue: {}", queueName);
    }

    private void publishToQueue(String message) {
        LOGGER.info("Publishing :{} to :{} queue", message, queueName);
        publisher.publishMessage(queueName, message);
        LOGGER.info("Published :{} to :{} queue", message, queueName);
    }

    private void writeToFile(UserSubscriptionVO userSubscriptionEntity) {
        try {
            String entityString = gson.toJson(userSubscriptionEntity);
            bufferedWriter.write(entityString);
            bufferedWriter.newLine();
            LOGGER.info("Stored :{} to :{} file", entityString, userSubscriptionEntity);
        } catch (IOException e) {
            LOGGER.error("Exception while processing record for entity {} exception: ", userSubscriptionEntity, e);
        }
    }

    private UserSubscriptionVO convertToUserSubscriptionVO(UserSubscriptionEntity userSubscriptionEntity) {
        LOGGER.info("Processing: {}", userSubscriptionEntity);
        UserSubscriptionVO userSubscriptionVO = UserSubscriptionVO.builder()
                .identity(userSubscriptionEntity.getUserId())
                .identityType(USER_ID)
                .source(BILLING_ENGINE)
                .sourceUniqueIdentifier(String.valueOf(userSubscriptionEntity.getId()))
                .startDate(userSubscriptionEntity.getSubscriptionStartDate().toInstant().toEpochMilli())
                .endDate(userSubscriptionEntity.getSubscriptionEndDate().toInstant().toEpochMilli())
                .partnerName(userSubscriptionEntity.getPartner().getDisplayName())
                .internalPartnerName(userSubscriptionEntity.getPartner().getName())
                .eventTime(userSubscriptionEntity.getLastUpdatedOn().getTime())
                .contentAccessAllowedOffNetwork(true)
                .planName("Premium")
                .build();
        if(BaasStatus.SUSPEND == userSubscriptionEntity.getBaasStatus() || BaasStatus.PARKING ==  userSubscriptionEntity.getBaasStatus()){
            userSubscriptionVO = userSubscriptionVO.toBuilder().planName("Basic").build();
        } else if("Digi_direct".equals(userSubscriptionEntity.getBillEndpoint())){
            userSubscriptionVO = userSubscriptionVO.toBuilder().planName("PremiumwithAds").build();
        }
        return userSubscriptionVO;
    }
}
