package com.vuclip.billing.engine.util.model;

/**
 * Currency 
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum Currency {
	INR("INR","India"), 
	IDR("IDR","Indonesia"), 
	MYR("MYR","Malaysia"), 
	USD("USD","Usa"), 
	SGD("SGD","Singapore"), 
	HKD("HKD","Hongkong"), 
	CNY("CNY","China"), 
	THB("THB","Thailand"), 
	AED("AED","UAE"), 
	EGP("EGP","Egypt"), 
	BHD("BHD","Bahrain"), 
	QAR("QAR","Qatar"),
	SAR("SAR","SaudiArabia"), 
	IQD("IQD","Iraq"), 
	FILS("FILS","Kuwait"), 
	KD("KD","Kuwait"),
	MMK("MMK", "Myanmar"),
	OMR("OMR", "Oman"),
	ZAR("ZAR", "SouthAfrica"),
	RAND("RAND", "SouthAfrica"),
	JOD("JOD", "Jordan");
	
	private String name;
	private String countryName;
	
	private Currency(String name, String countryName){
		this.name=name;
		this.countryName=countryName;
	}
	
	public String getName() {
		return name;
	}	
	public String getCountryName() {
		return countryName;
	}
}
