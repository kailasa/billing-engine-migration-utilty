package com.vuclip.billing.engine.util;

import com.vuclip.billing.engine.util.service.MigrationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StopWatch;

@SpringBootApplication
@ComponentScan({"com.vuclip.rabbitmq","com.vuclip.billing.engine"})
public class Application implements CommandLineRunner {

    private static final Logger LOGGER = LogManager.getLogger(Application.class.getName());

    private final MigrationService service;

    @Value("${operationtype:FETCH}")
    private String operationtype;

    @Autowired
    public Application(MigrationService service) {
        this.service = service;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args).close();
    }

    @Override
    public void run(String... args) {

        LOGGER.info("Running Utility to push Billing Engine User Subscriptions to Privilege Service");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        if(operationtype.equalsIgnoreCase("FETCH")) {
            service.fetch();
        } else {
            service.publish();
        }

        stopWatch.stop();
        LOGGER.info("Utility completed successfuly in {} seconds.", stopWatch.getTotalTimeSeconds());
    }
}
