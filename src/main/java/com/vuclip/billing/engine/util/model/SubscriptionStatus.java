package com.vuclip.billing.engine.util.model;
/**
 * Subscription status
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum SubscriptionStatus {

	ACTIVE("Active"), GRACE("Grace"), SUSPEND("Suspend"), INACTIVE("Inactive"),
	PENDING("Pending"), PARKING("Parking"), TRIAL("Trial"), TRIAL_EXPIRED("Trial Expired");


	private String name;

	private SubscriptionStatus(String name){
		this.name=name;
	}

	public String getName() {
		return name;
	}
}
