package com.vuclip.billing.engine.util.model;

/**
 * All Partner
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum Partner {


	FREECHARGE("Freecharge","com.viu.billing.partner.IFreechargeBillingService", Country.IN, false, "FreeCharge"),
	PAYTM("Paytm","com.viu.billing.partner.IPaytmBillingService", Country.IN, false, "Paytm"),
	GOOGLE("Google","com.viu.billing.partner.IGoogleBillingService", null, false, "Google"), 
	APPLE("Apple","com.viu.billing.partner.IAppleBillingService", null, false, "Apple"), 
	CODAPAY("Codapay","com.viu.billing.partner.ICodapayBillingService", null, false, "Codapay"),
	MANDIRI("Mandiri","com.viu.billing.partner.IMandiriBillingService", Country.ID, false, "Mandiri"),
	TELEKOMMALAYSIA("TelekomMalaysia","com.viu.billing.partner.ITelekomMalaysiaBillingService", Country.MY, false, "Telekom Malaysia"),
	IN_APP_PURCHASE("In App Purchase","com.viu.billing.partner.IAppleBillingService", null, false, "In App Purchase"), // only for ios migration purpose.
	MOLPAY("MolPay", "com.viu.billing.partner.IMolPayBillingService", Country.MY, false, "Molpay"),
	MAXIS("maxis","com.viu.billing.partner.IMaxisBillingService", Country.MY, true, "Maxis"),
	UMOBILE("umobile","com.viu.billing.partner.IUmobileService", Country.MY, true, "umobile"),
	AIRTEL("Airtel","com.viu.billing.partner.IAirtelService", Country.IN, true, "Airtel"),
	ORANGEEGYPT("OrangeEgypt","com.viu.billing.partner.IEgyptOrangeBillingService", Country.EG, true, "Orange Egypt"),
	ETISALAT_EGYPT("ETISALAT_EGYPT","com.viu.billing.partner.IEtisalatBillingService", Country.EG, true, "Etisalat Egypt"),
	DIGI("Digi","com.viu.billing.partner.IDigimalaysiaBillingService", Country.MY, true, "Digi"),
	VODAFONE("Vodafone","com.viu.billing.partner.IVodafoneService", Country.IN, true, "Vodafone"),
	CELCOM("Celcom","com.viu.billing.partner.ICelcomService", Country.MY, true, "Celcom"),
	ZAIN("Zain","com.viu.billing.partner.IZainService", Country.IQ, true, "Zain"),
	ZAINKW("ZainKw","com.viu.billing.partner.IZainKwService", Country.KW, true, "Zain Kuwait"),
	ASIACELL("AsiaCell","com.viu.billing.partner.IAsiaCellService", Country.IQ, true, "AsiaCell"),
    STC("Stc","com.viu.billing.partner.IstcService", Country.SA, true, "STC"),
	VIVAKUWAIT("VivaKuwait","com.viu.billing.partner.IVivaKuwaitService", Country.KW, true, "Viva Kuwait"),
	DU_UAE("DU_UAE","com.viu.billing.partner.IDuUAEService", Country.AE, true, "DU UAE"),
	ETISALAT("ETISALAT","com.viu.billing.partner.IEtisalatUAEService", Country.AE, true, "ETISALAT"),
	ZAIN_BAHRAIN("ZAIN_BAHRAIN","com.viu.billing.partner.IZainBahrainService", Country.BH, true, "ZAIN BAHRAIN"),
	OOREDOO_QATAR("OOREDOO_QATAR","com.viu.billing.partner.OoredooQatarService", Country.QA, true, "OOREDOO QATAR"),
	OMANTEL("OMANTEL","com.viu.billing.partner.OmantelOmanService", Country.OM, true, "OMANTEL"),
	IDEA("Idea","com.viu.billing.partner.IIdeaService", Country.IN, true, "Idea"),
	OOREDOO_OMAN("OOREDOO_OMAN","com.viu.billing.partner.IOoredooOmanService", Country.OM, true, "OOREDOO OMAN"),
	VODAFONEEGYPT("VodafoneEgypt","com.viu.billing.partner.IVodafoneEgyptService", Country.EG, true, "Vodafone Egypt"),
	OOREDOO_KUWAIT("OOREDOO_KUWAIT","com.viu.billing.partner.IOoredooKuwaitService", Country.KW, true, "OOREDOO KUWAIT"),
	MOBIKWIK("MobiKwik", "com.viu.billing.partner.IMobikwikBillingService", Country.IN, false, "MobiKwik"),
	PAYPAL("PayPal", "dummyClass", null, false, "PayPal"),
	VODACOM("Vodacom","com.viu.billing.partner.IVodacomViuService", Country.ZA, true, "Vodacom"),
	TELR("Telr", "dummyClass", null, false, "Telr"),
	CODAPAY_RENEWAL("CODAPAY_RENEWAL", "com.viu.billing.partner.ICodapayBillingService", null, false, "Codapay"),
	ECENTRIC("Ecentric", "dummyClass", null, false, "Credit card/Debit card");
	
	private String name;
	private String className;
	private Country defaultCountry;
	private boolean isCarrier;
	private String displayName;
	
	private Partner(String name, String className, Country defaultCountry, boolean isCarrier, String displayName){
		this.name=name;
		this.className=className;
		this.setDefaultCountry(defaultCountry);
		this.isCarrier=isCarrier;
		this.displayName = displayName;
	}
	
	public String getName() {
		return name;
	}

	public String getClassName() {
		return className;
	}

	public Country getDefaultCountry() {
		return defaultCountry;
	}

	private void setDefaultCountry(Country defaultCountry) {
		this.defaultCountry = defaultCountry;
	}
	
	public static Partner findByValue(String value) {
		for(Partner partner : values()) {
			if(partner.getName().equals(value)) {
				return partner;
			}
		}
		return null;
	}

	public boolean isCarrier() {
		return isCarrier;
	}

	public String getDisplayName() {
		return displayName;
	}

}
