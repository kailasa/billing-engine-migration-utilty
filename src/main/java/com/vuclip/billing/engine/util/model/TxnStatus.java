package com.vuclip.billing.engine.util.model;
/**
 * Enum to hold txn status
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum TxnStatus {
	
	PENDING("pending"), SUCCESS("success"), FAILED("failed"), DELETED("Forced deleted"), EXPIRED("Forced expired"), CONFIRMED("confirmed");

	private String name;
	
	private TxnStatus(String name){
		this.name=name;
	}
	
	public String getName() {
		return name;
	}
}
