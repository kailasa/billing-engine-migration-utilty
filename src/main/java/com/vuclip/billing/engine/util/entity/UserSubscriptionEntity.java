package com.vuclip.billing.engine.util.entity;

import com.vuclip.billing.engine.util.model.BaasStatus;
import com.vuclip.billing.engine.util.model.Partner;
import com.vuclip.billing.engine.util.model.RecordType;
import com.vuclip.billing.engine.util.model.SubscriptionStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="billing_user_subscription")
@Getter
@Setter
public class UserSubscriptionEntity extends RdbmsEntity{

	private static final long serialVersionUID = 7360514986433510230L;
	@Column(name="user_id", nullable=false, length=50)
	private String userId;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "subscription_id")
	private SubscriptionPlanEntity subscriptionPlan;
	@Column(name="subscription_status", nullable=false)
	@Enumerated(EnumType.STRING)
	private SubscriptionStatus subscriptionStatus;
	@Column(name="partner", length=50)
	@Enumerated(EnumType.STRING)
	private Partner partner;
	@Column(name="sub_start_date")
	private Date subscriptionStartDate;
	@Column(name="sub_end_date")
	private Date subscriptionEndDate;
	@Column(name="msisdn", length=30)
	private String msisdn;
	@OneToOne(cascade = CascadeType.ALL, mappedBy="userSubscriptionEntity", fetch=FetchType.LAZY)
	private UserPartnerTokenEntity userPartnerToken;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="userSubscription", fetch=FetchType.LAZY)
	private Set<OrderEntity> orders=new HashSet<>();
	@Column(name="backend_aware")
	private boolean isBackendAware=false;
	@Column(name="renewal_failed_count")
	private short renewalFailedCount=0;
	@Column(name="appid")
	private String appId;
	//1.current 2.history
	@Column(name="record_type", nullable=false, length=15)
	@Enumerated(EnumType.STRING)
	private RecordType recordType;
	
	@OneToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "current_order", referencedColumnName = "id")
	private OrderEntity currentOrder;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "current_txn", referencedColumnName = "id")
	private ViuTxnEntity currentTxn;

	@Column(name="bill_endpoint")
	private String billEndpoint;
	
	@Column(name="dirt_tid")
	private String dirtTid;
	
	@Column(name="sub_type")
	private String subType;
	
	@Column(name="baas_status")
	@Enumerated(EnumType.STRING)
	private BaasStatus baasStatus;
	
	@Column(name = "migrated_to_ubs", nullable = false)
	@Type(type = "yes_no")
	private boolean migratedToUBS;


}
