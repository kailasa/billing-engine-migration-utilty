package com.vuclip.billing.engine.util.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="billing_user_partner_token")
public class UserPartnerTokenEntity implements Serializable {

	private static final long serialVersionUID = 7583928920389012125L;

	@Id
	@Column(name="id")
	private Long userSubscriptionId;

	@Lob
    @Basic(fetch=FetchType.LAZY)
	@Column(name="token", nullable=false)
	private String token;
	
	@Basic(fetch=FetchType.LAZY)
	@Column(name="transaction_id")
	private String transactionId;
	
	@JoinColumn(name = "id")
    @OneToOne
    @MapsId
	private UserSubscriptionEntity userSubscriptionEntity;
   
}
