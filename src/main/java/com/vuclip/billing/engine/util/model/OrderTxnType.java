package com.vuclip.billing.engine.util.model;

/**
 * OrderTxnType
 * 
 * @author pranav.tiwary@vuclip.com
 * 
 * From 3rd dec 2017, there will be two column txn_type and next_txn_type
 * +-------------------------------+
| txn_type                      |
+-------------------------------+
| RENEWAL_BE_PULL               |
| RENEWAL_BE_PULL_FAILED        |
| RENEWAL_BASS_PUSH             |
| RENEWAL_PARTNER_PUSH          |
| RENEWAL_BE_PULL_PENDING		|
| NEW_REGISTRATION              |
| NEW_USER_VERIFICATION_FAILED  |
| NEW_USER_VERIFICATION_SUCCESS |
| NEW_USER_BAAS_PUSH_PENDING	|
| RENEWAL_BAAS_PUSH_PENDING		|
+-------------------------------+


+-------------------------------------+
| next_txn_type                       |
+-------------------------------------+
| RENEWAL                             |
| RENEWAL_BE_PULL_INACTIVE            |
| RENEWAL_BE_PULL_FAILED              |
| DEACTIVATION_BASS_PUSH              |
| NULL                                |
| USER_DEACTIVATED                    |
| PARTNER_DEACTIVATED                 |
| RENEWAL_BE_PULL_INACTIVE_FIX_RECORD |
+-------------------------------------+
 *
 */
public enum OrderTxnType {

	NEW_REGISTRATION_VIA_MM, // new registration via mass market
	NEW_REGISTRATION, // new registration
	MIGRATION, // migrated data

	RENEWAL,// Renewal this shuld be always in next txn type column Just to mark what happened next, This will be in column next_txn_type
	RENEWAL_BASS_PUSH, // renewal push from baas
	RENEWAL_INTERACTIVE_BE_PULL,// interactive renewal billing engine pull
	RENEWAL_BE_PULL, // Billing engine pulled
	RENEWAL_BE_PULL_INACTIVE_FIX_RECORD, // fix the record if there was two active order same time, This will be in column next_txn_type
	RENEWAL_BE_PULL_NEXT_SUB, // Billing engine pulled to next subscription, must hv a new entry above this . USE RENEWAL INSTEAD .Not used now
	RENEWAL_PARTNER_PUSH, // partner pushed for renewal
	RENEWAL_BE_PULL_FAILED,  // we tried renewal but it failed due to various reason, may be due to txn failure,This will be in column next_txn_type and txn_type
	RENEWAL_BE_PULL_PENDING,	// we tried for renewal but in baas it was in suspend or grace status, so renewal is in pending status

	RENEWAL_BE_PULL_INACTIVE, // Billing engine pulled but inactive.  This will be in column next_txn_type
	USER_DEACTIVATED, // user deactivated, This will be in column next_txn_type
	PARTNER_DEACTIVATED, // partner deactivated, This will be in column next_txn_type
	DEACTIVATION_BASS_PUSH, // Unsub from baas push, This will be in column next_txn_type
	RENEWAL_PARTNER_PUSH_INACTIVE, // partner pushed for renewal but inactive (expire end date was provided). Just for Apple Notification case.

	NEW_USER_BAAS_PUSH, // Same as NEW_REGISTRATION but baas pushed it and it was unavailable, may be confirmation call missing
	NEW_USER_VERIFICATION_SUCCESS_BAAS_PUSH, //  new user verification is success Same as NEW_REGISTRATION but baas pushed it
	NEW_USER_VERIFICATION_SUCCESS, // new user verification is success Same as NEW_REGISTRATION, but BEngine pulled it
	NEW_USER_VERIFICATION_FAILED, // new user verification failed
	S2S_TXN_VERIFICATION_SUCCESS, // txn verification success by s2s call
	S2S_TXN_VERIFICATION_FAILED, // txn verification failed by s2s call
	NEW_USER_BAAS_PUSH_PENDING, // charging notification from baas for parking user
	RENEWAL_BAAS_PUSH_PENDING 	// renewal notification from baas for suspend or grace user

}
