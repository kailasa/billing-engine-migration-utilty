package com.vuclip.billing.engine.util.repository;

import com.vuclip.billing.engine.util.entity.UserSubscriptionEntity;
import com.vuclip.billing.engine.util.model.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface UserSubscriptionRepository extends JpaRepository<UserSubscriptionEntity, Long> {

    @QueryHints(@javax.persistence.QueryHint(name = "org.hibernate.fetchSize", value = "5000"))
    List<UserSubscriptionEntity> findByPartnerAndSubscriptionEndDateGreaterThanEqual(Partner partner, Date date);
}