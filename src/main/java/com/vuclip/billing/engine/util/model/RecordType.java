package com.vuclip.billing.engine.util.model;
/**
 * RecordType : Enum
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum RecordType {
	
	CURRENT("current"), HISTORY("history");
	
	private String name;
	
	private RecordType(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
}
