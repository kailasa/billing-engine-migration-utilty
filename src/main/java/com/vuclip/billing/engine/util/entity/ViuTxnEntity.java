package com.vuclip.billing.engine.util.entity;

import com.vuclip.billing.engine.util.model.Country;
import com.vuclip.billing.engine.util.model.Currency;
import com.vuclip.billing.engine.util.model.Partner;
import com.vuclip.billing.engine.util.model.TxnStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="billing_viu_txn")
public class ViuTxnEntity extends RdbmsEntity{
	
	private static final long serialVersionUID = -6659271213761551194L;

	@Column(name="user_id", length=50,  nullable=false)
	private String userId;
	
	@Column(name="phone_number", length=30)
	private String phoneNumber;
	@Column(name="partner_name", nullable=false, length=50)
	@Enumerated(EnumType.STRING)
	private Partner partner;
	
	@Column(name="txn_status", nullable=false, length=15)
	@Enumerated(EnumType.STRING)
	private TxnStatus txnStatus;
	
	@Column(name="country", length=15)
	@Enumerated(EnumType.STRING)
	private Country country;
	
	@Column(name="currency", length=15)
	@Enumerated(EnumType.STRING)
	private Currency currency;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="order_id_dynamo", length=50)
	private String orderIdDynamo;
	
	@Column(name="backend_aware")
	private boolean isBackendAware=false;
	
	@OneToOne(mappedBy="txn", fetch=FetchType.LAZY)
	private OrderEntity order;

}