package com.vuclip.billing.engine.util.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
@MappedSuperclass
@Getter
@Setter
public abstract class RdbmsEntity implements Serializable{

	@Id
	protected Long id;

	@Column(updatable=false)
	protected Date createdOn;

	@Column(name="last_updated_on")
	protected Date lastUpdatedOn;
}
