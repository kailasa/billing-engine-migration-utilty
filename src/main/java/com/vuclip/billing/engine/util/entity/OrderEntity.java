package com.vuclip.billing.engine.util.entity;


import com.vuclip.billing.engine.util.model.OrderTxnType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name="billing_order")
public class OrderEntity extends RdbmsEntity{

	private static final long serialVersionUID = 3434469064408058557L;
	@Column(name="order_id_dynamo", length=50)
	private String orderIdDynamo;
	@ManyToOne(optional=false,fetch=FetchType.LAZY)
	@JoinColumn(name = "user_subscription_id", updatable=false)
	private UserSubscriptionEntity userSubscription;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "txn_id",  unique=true)
	private ViuTxnEntity txn;
	@Column(name="userId", nullable=false, length=50)
	private String userId;
	@Column(name="active", nullable=false)
	private boolean active;
	@Column(name="sub_start_date")
	private Date subscriptionStartDate;
	@Column(name="sub_end_date")
	private Date subscriptionEndDate;
	@Column(name="Txn_Type")
	@Enumerated(EnumType.STRING)
	private OrderTxnType orderTxnType;
	@Column(name="targeted_sub_plan_id")
	private Long targetedSubPlanId;
	@Column(name="charged_sub_plan_id")
	private Long chargedSubPlanId;
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "current_plan_id")
	private SubscriptionPlanEntity currentPlan;
	@Column(name="next_txn_type")
	@Enumerated(EnumType.STRING)
	private OrderTxnType nextTxnType;
	@Column(name="next_txn_type_date")
	private Date nextTxnTypeDate;
}
