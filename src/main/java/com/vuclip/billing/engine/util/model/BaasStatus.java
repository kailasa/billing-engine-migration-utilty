package com.vuclip.billing.engine.util.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;


@Getter
@ToString
@AllArgsConstructor
public enum BaasStatus {

	ACT_INIT(1),		// ACTIVATION_INITIATION
	ACT_ERROR(2),		// ACTIVATION ERROR. END STATE.
	ACT_RETRY(3),		// ACTIVATION RETRY
	ACTIVATED(4),		// ACTIVATED
	PARKING(5),			// Activation Low-Balance. Only in case of new ACTIVATION.
	GRACE(6),			// Renew Low-Balance. Only in case of renewal.
	SUSPEND(7),			// Renew Low-Balance. Only in case of renewal.
	DEACT_INIT(8),		// DeActivation Initiation.
	DEACT_RETRY(9),		// DeActivation retry.
	DEACT_ERROR(10),	// DeActivation error.
	DEACTIVATED(11);	// DeActivation. End state.

	int statusId;
}
