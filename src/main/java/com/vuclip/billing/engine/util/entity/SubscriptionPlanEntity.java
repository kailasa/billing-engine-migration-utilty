package com.vuclip.billing.engine.util.entity;

import com.vuclip.billing.engine.util.model.Country;
import com.vuclip.billing.engine.util.model.Currency;
import com.vuclip.billing.engine.util.model.Partner;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="billing_subscription_plan", uniqueConstraints= @UniqueConstraint(columnNames={"billing_code", "country","partner", "amount" }))
@Setter
public class SubscriptionPlanEntity extends RdbmsEntity{

	private static final long serialVersionUID = -8441137861257225744L;

	public static final String DEFAULT = "Default";
	public static final int DEFAULT_DAYS = 30;
	
	@Column(name="billing_code", nullable=false, length=50)
	private String billingCode;
	@Column(name="amount", nullable=false)
	private Double amount;
	@Column(name="validity_days")
	private int validityDays;
	@Column(name="active", nullable=false)
	private boolean active;
	@Column(name="country", length=15)
	@Enumerated(EnumType.STRING)
	private Country country;
	@Column(name="currency", length=15)
	@Enumerated(EnumType.STRING)
	private Currency currency;
	@Column(name="partner", length=50)
	@Enumerated(EnumType.STRING)
	private Partner partner;
}
