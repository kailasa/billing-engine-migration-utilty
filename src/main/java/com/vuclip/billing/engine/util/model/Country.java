package com.vuclip.billing.engine.util.model;
/**
 * Hold country details
 * 
 * @author pranav.tiwary@vuclip.com
 *
 */
public enum Country {
	
	IN("IN","India",Currency.INR), 
	MY("MY","Malaysia",Currency.MYR), 
	ID("ID","Indonesia",Currency.IDR), 
	US("US","Usa",Currency.USD), 
	CN("CN","China",Currency.USD), 
	TH ("TH","Thailand", Currency.USD), 
	SG("SG","Singapore", Currency.USD), 
	HK("HK","Hongkong",Currency.USD),
	AE("AE","UAE",Currency.AED), 
	EG("EG","Egypt",Currency.EGP), 
	BH("BH","Bahrain",Currency.BHD), 
	QA("QA","Qatar",Currency.QAR),
	KW("KW","Kuwait",Currency.KD), 
	JO("JO","Jordan", Currency.JOD), 
	OM("OM","Oman",Currency.OMR), 
	SA("SA","SaudiArabia",Currency.SAR), 
	IQ("IQ","Iraq",Currency.IQD),
    MM("MM", "Myanmar", Currency.MMK),
	ZA("ZA", "SouthAfrica", Currency.ZAR);
	
	private String name;
	private String countryName;
	private Currency currency;
	
	private Country(String name, String countryName, Currency currency){
		this.name=name;
		this.countryName=countryName;
		this.currency=currency;
	}
	
	public String getName() {
		return name;
	}

	public String getCountryName() {
		return countryName;
	}
	
	public Currency getCurrency() {
		return currency;
	}
}
